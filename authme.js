const { token, shardcount, clustercount, guildspershard } = require("./utils/variables");
const { ShardingManager } = require('kurasuta');
const { join } = require('path');
const sharder = new ShardingManager(join(__dirname, 'bot'), {
    token: token,
    shardCount: shardcount,
    clusterCount: clustercount,
    respawn: true,
    guildsPerShard: guildspershard,
    clientOptions: {
        disabledEvents: ["TYPING_START", "VOICE_SERVER_UPDATE", "VOICE_STATE_UPDATE", "PRESENCE_UPDATE"]
    },
    ipcSocket: 9994
});

sharder.spawn();