const Sequelize = require('sequelize');

const db = new Sequelize('authme', 'authme', "$Hc1#8h&@hDe", {
    host: "db.dropletdev.com",
    dialect: "mysql",
    dialectOptions: {
        compress: true
    },
    logging: false,
    pool: {
        max: 5,
        min: 0,
        idle: 200000,
        acquire: 200000
    }
})


// Export
module.exports = db;