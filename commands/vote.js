const { MessageEmbed } = require("discord.js");
const { guilds, users } = require("../utils/variables");

exports.use = async (client, message, args, command) => {
    let voteEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe - Vote")
        .setDescription("Voting helps us out alot! Want to vote? Click [here](https://discordbots.org/bot/550613223733329920/vote)")
        //.addField("Why vote?", "Voting gives you some nice perks! Here is a list:\n-No command cooldown for 12 hours\n-Make unlimited AuthMe for 12 hours\n\nVoting is free and so is it to use the bot, so don't come yell at us for making this limitation")
        .addField("Why vote?", "Voting gives you some nice perks! Here is a list:\n-No command cooldown for 12 hours\nVoting is free and so is it to use the bot, so don't come yell at us for making this limitation. \nThere will be more bonuses soon!")
        .setURL("https://discordbots.org/bot/550613223733329920/vote")
        .setFooter(guilds.get(message.guild.id).footer);

    message.channel.send(voteEmbed).catch(err => { });
};

exports.command = {
    aliases: [""]
}