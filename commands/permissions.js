const { MessageEmbed } = require("discord.js");
const { guilds } = require("../utils/variables");
const { Roles, Servers, Votes } = require("../database/db");

exports.use = async (client, message, args, command) => {
    let botUser = message.guild.members.get(client.user.id);
    let permissionEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setFooter(guilds.get(message.guild.id).footer)
        .setTitle("AuthMe - Permission Check")
        .setDescription("It is reccomended to give me administrator, but below I have listed all permissions I need to have no matter what to function corretly. If it shows as \`true\` I have the permission but if it shows as \`false\` I am missing the permission. You also \`NEED\` to move me higher than the role you are trying to give in the role hiarcy")
        .addField("Administrator", botUser.hasPermission("ADMINISTRATOR"))
        .addField("Manage Roles", botUser.hasPermission("MANAGE_ROLES"))
        .addField("Embed Links", botUser.hasPermission("EMBED_LINKS"))
        .addField("Send Messages", botUser.hasPermission("SEND_MESSAGES"))
        //.addField("Add Reactions", botUser.hasPermission("ADD_REACTIONS"))
        .addField("Manage Channels", botUser.hasPermission("MANAGE_CHANNELS"))
        .addField("Read Messages", botUser.hasPermission("READ_MESSAGES"))
        .addField("Manage Messages", botUser.hasPermission("MANAGE_MESSAGES"))
        .addField("Read Message History", botUser.hasPermission("READ_MESSAGE_HISTORY"))

    message.channel.send(permissionEmbed).catch(err => { });
};

exports.command = {
    aliases: ["perms", "checkperms"]
};