const { MessageEmbed } = require("discord.js");
const { Servers, Premium } = require("../database/db");
const axios = require("axios");
exports.use = async (client, message, args, command) => {
    let key = args[0]
    if (!key) return message.reply("Please supply me with a valid key.")
    let reponse = await axios.post(`https://store.dropletdev.com/api/checkKey/authme/${key}/${message.guild.id}/${message.author.id}`, {}, {
        headers: {
            'Authorization': 'Bearer Zq4t7w!z%C*F-JaNdRgUkXp2s5u8x/A?D(G+KbPeShVmYq3t6w9y$B&E)H@McQfTjWnZr4u7x!A%C*F-JaNdRgUkXp2s5v8y/B?E(G+KbPeShVmYq3t6w9z$C&F)J@McQfTjWnZr4u7x!A%D*G-KaPdRgUkXp2s5v8y/B?E(H+MbQeThVmYq3t6w9z$C&F)J@NcRfUjXnZr4u7x!A%D*G-KaPdSgVkYp3s5v8y/B?E(H+MbQeThWmZq4t7w9z$C&'
        }
    });
    if (!reponse) return message.reply("Some error occured, please contact droplet staff via the support server.");
    if (!reponse.data) return message.reply("Some error occured, please contact droplet staff via the support server.");
    let enabled = reponse.data.success;
    if (!enabled) return message.reply("This key does not exist or it is redeemed!");
    Premium.create({ code: key, redeemed: true, length: reponse.data.length, expires: reponse.data.expires, guildid: message.guild.id, clientid: message.author.id });
    Servers.update({ premium: true }, { where: { guildid: message.guild.id } });
    message.reply("You have enabled premium!");
};

exports.command = {
    aliases: [""]
};