const Sequelize = require('sequelize');
const db = require('./connection');

const Roles = db.define('roles', {
    guildid: {
        type: Sequelize.STRING,
    },
    logschannel: {
        type: Sequelize.STRING,
        defaultValue: "authlogs"
    },
    joinrole: {
        type: Sequelize.STRING,
        defaultValue: "Unverified"
    },
    joinmessage: {
        type: Sequelize.STRING,
        defaultValue: "Hello! Welcome to this server! To prove you are not a bot, please type am!verify in the verification channel to receive a CAPTCHA to solve in this DM."
    },    
    joinchannel: {
        type: Sequelize.STRING,
        defaultValue: "verification"
    },
    joinchannelid:{
        type: Sequelize.STRING
    },
    logschannelid:{
        type: Sequelize.STRING
    },
    givenRole:{
        type: Sequelize.STRING
    },
    givenRoleid:{
        type: Sequelize.STRING
    }
    
});

module.exports = Roles;