const { MessageEmbed } = require("discord.js");
const fs = require("fs");
const { token, guilds, users, roles } = require("./utils/variables.js");
const { checkDate } = require("./utils/functions");
process.setMaxListeners(0);
const { BaseCluster } = require('kurasuta');
const { Roles, Servers } = require("./database/db");
const DBL = require("dblapi.js");
const dbl = new DBL("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU2MDQ1NzE0NDUxMjY3NTg0NiIsImJvdCI6dHJ1ZSwiaWF0IjoxNTcwMDE1OTc4fQ.hLaHt2KdGDPSD2mvYmuLDdAacOTUlYKpbLZcI5maJik", { webhookPort: 5012, webhookAuth: 'audwhjaowfdhawipdkawd' });

module.exports = class extends BaseCluster {
    launch() {
        this.client.login(token)
        this.client.on("shardReady", (id) => console.log(`Shard ${id} is online.`))
        this.client.once("ready", async () => {
            this.client.user.setActivity(`am!help || Droplet`);
            setInterval(() => {
                this.client.user.setActivity(`am!help || Droplet`);
            }, 60000 * 10);
            setInterval(() => {
                checkDate();
            }, 60 * 60 * 1000)
            if (this.id == 0) {
                setInterval(() => {
                    const promises = [
                        this.client.shard.fetchClientValues('guilds.size'),
                    ];
                    Promise.all(promises).then(results => {
                        if (!results) return;
                        const totalGuilds = results[0].reduce((prev, guildCount) => prev + guildCount, 0);
                        dbl.postStats(totalGuilds);
                    });
                }, 180000);
            }
        });

        this.client.commands = new Map();
        this.client.aliases = new Map();
        let talkedRecently = new Set();

        fs.readdir('./commands/', (err, files) => {
            let jsfiles = files.filter(f => f.split('.')
                .pop() === 'js');
            if (jsfiles.length <= 0) {
                return;
            }
            jsfiles.forEach(f => {
                let props = require(`./commands/${f}`);
                props.fileName = f;
                this.client.commands.set(f.slice(0, -3), props);
                props.command.aliases.forEach(alias => {
                    this.client.aliases.set(alias, f.slice(0, -3));
                });
            });
            console.log("Loaded " + jsfiles.length + " commands.")
        });

        this.client.on("message", async message => {
            if (message.author.bot) return;
            if (!message.guild) return;
            //if(message.includes('shut up') && message.server.id == "550516279652515880") return message.channel.send("No you " + message.author.name);
            Roles.findOne({ where: { guildid: message.guild.id } }).then(data => {
                if (!data) message.reply("Your server is not registered in my database! Please re-invite me! If that does not fix it please join my support server! Link: https://dropletdev.com/discord").catch(err => { });
                let info = data.dataValues;
                let rolesObj = {
                    joinmessage: info.joinmessage,
                    joinrole: info.joinrole,
                    logschannel: info.logschannel,
                    joinchannel: info.joinchannel
                };
                roles.set(message.guild.id, rolesObj);
            });
            Servers.findOne({ where: { guildid: message.guild.id } }).then(data => {
                if (!data) return guild.leave().catch(err => { });
                let info = data.dataValues;
                let dms;
                let premium;
                let setup;
                let authlogs;
                if (info.dms == 0) dms = false;
                if (info.dms == 1) dms = true;
                if (info.setup == 0) setup = false;
                if (info.setup == 1) setup = true;
                if (info.premium == 0) premium = false;
                if (info.premium == 1) premium = true;
                if (info.authlogs == 0) authlogs = false;
                if (info.authlogs == 1) authlogs = true;
                let guildObj = {
                    footer: info.footer,
                    embedColor: info.embedcolor,
                    dms: dms,
                    premium: premium,
                    prefix: info.prefix,
                    setup: setup,
                    authlogs: authlogs,
                };
                guilds.set(message.guild.id, guildObj);
                let prefix;
                if (guilds.get(message.guild.id)) {
                    if (guilds.get(message.guild.id).prefix) prefix = guilds.get(message.guild.id).prefix;
                }
                let prefixArray = ["<@560457144512675846>", "<@!560457144512675846>", "am!", "AM!", "aM!", "Am!", prefix];
                for (const thisPrefix of prefixArray) {
                    if (message.content.startsWith(thisPrefix)) prefix = thisPrefix;
                };
                if (message.channel.name == roles.get(message.guild.id).joinchannel && message.author.id != 560457144512675846) {
                    if (!message.deleted) message.delete()
                }
                if (!message.content.startsWith(prefix)) return;
                const args = message.content.slice(prefix.length).trim().split(/ +/g);
                let command = args.shift().toLowerCase();
                let cmd;
                if (this.client.commands.has(command)) {
                    cmd = this.client.commands.get(command);
                } else if (this.client.aliases.has(command)) {
                    cmd = this.client.commands.get(this.client.aliases.get(command));
                }
                if (!cmd) return;
                if (guilds.get(message.guild.id).premium) {////delete the !from this soon
                    cmd.use(this.client, message, args, command);
                } else {
                    let cooldownSec = 4;
                    if (talkedRecently.has(message.author.id)) {
                        let talkedRecentlyEmbed = new MessageEmbed()
                            .setColor(guilds.get(message.guild.id).embedColor)
                            .setTitle("You are on cooldown!")
                            .setDescription("Please wait up to 8 seconds to use another command! By voting or getting premium you will have this restriction removed for 12 hours, by getting premium this is removed permanently.")
                            .setFooter(guilds.get(message.guild.id).footer);

                        return message.channel.send(talkedRecentlyEmbed).catch(err => { });
                    }
                    talkedRecently.add(message.author.id);
                    cmd.use(this.client, message, args, command);
                    setTimeout(() => {
                        talkedRecently.delete(message.author.id);
                    }, cooldownSec * 1000)
                }
            })
        });

        this.client.on("guildCreate", async guild => {
            Servers.findOne({ where: { guildid: guild.id } }).then(res => {
                if (!res) {
                    Servers.create({ guildid: guild.id });
                }
            })

            Roles.findOne({ where: { guildid: guild.id } }).then(res => {
                if (!res) {
                    Roles.create({ guildid: guild.id });
                }
            })

        });

        this.client.on("guildMemberAdd", async member => {
            let guild = member.guild;
            Roles.findOne({ where: { guildid: guild.id } }).then(data => {
                if (!data) message.reply("Your server is not registered in my database! Please re-invite me! If that does not fix it please join my support server! Link: https://dropletdev.com/discord").catch(err => { });
                let info = data.dataValues;
                let rolesObj = {
                    joinmessage: info.joinmessage,
                    joinrole: info.joinrole,
                    logschannel: info.logschannel,
                    joinchannel: info.joinchannel
                };
                roles.set(guild.id, rolesObj);
            });

            Servers.findOne({ where: { guildid: guild.id } }).then(data => {
                if (!data) {
                    message.reply("Guild not found").catch(err => { });
                    guild.leave();
                }


                let info = data.dataValues;
                if(info.setup){
                    if(guilds.get(member.guild.id).dms) member.send(roles.get(member.guild.id).joinmessage)
                    let role = member.guild.roles.find(r => r.name == roles.get(member.guild.id).joinrole);
                    member.roles.add(role);
                }
            });
        });

        this.client.on("guildMemberRemove", async member => {
            let userObj = {
                usingverify: undefined,
            };
            users.set(member.id, userObj)
        });

        this.client.on("channelCreate", async channel => {
            if (!channel.guild) return;
            Servers.findOne({ where: { guildid: channel.guild.id } }).then(data => {
                if (!data) {
                    message.reply("Guild not found").catch(err => { });
                    guild.leave();
                }


                let info = data.dataValues;
                if (info.setup) {
                    channel.updateOverwrite(channel.guild.roles.find(role => role.name === joinRole), {
                        VIEW_CHANNEL: false
                    })
                    channel.updateOverwrite(this.client.user, {
                        VIEW_CHANNEL: true,
                        SEND_MESSAGES: true
                    })
                }
            });
        });

    }
};