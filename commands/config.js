const { MessageEmbed } = require("discord.js");
const { Roles, Servers } = require("../database/db");
const { guilds } = require("../utils/variables");
exports.use = async (client, message, args, command) => {
    let noPremiumEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("Authme - Premium Needed")
        .setDescription("You need premium to use this command!")
        .setFooter(guilds.get(message.guild.id).footer);

    if (!guilds.get(message.guild.id).premium) return message.channel.send(noPremiumEmbed).catch(err => { });

    let noPermissionEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("Authme - Missing Permissions")
        .setDescription("You are missing permissions to run this command.")
        .setFooter(guilds.get(message.guild.id).footer);
    if (!message.member.hasPermission("ADMINISTRATOR")) return message.channel.send(noPermissionEmbed).catch(err => { });

    let configHelpEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("Authme - Bot configuration")
        .addField("embedcolor", "Set the embed color of all embeds")
        .addField("footer", "Change the footer of every embed")
        .addField("prefix", "Change the default bot prefix")
        .addField("directmessages", "Enable/Disable direct messages from the bot")
        .addField("authlogs", "Enable/Disable logs from the bot")
        .addField("joinmessage", "The message that you will get when joining.")
        .setFooter(guilds.get(message.guild.id).footer);
    let validArray = ["embedcolor", "footer", "prefix", "directmessages", "joinrole", "joinmessage", "authlogs"];
    if (!validArray.includes(args[0]) || !args[0]) return message.channel.send(configHelpEmbed).catch(err => { });

    if (args[0] == "prefix") {
        let correctPrefixEmbed = new MessageEmbed()
            .setColor(guilds.get(message.guild.id).embedColor)
            .setTitle("Authme - Wrong usage!")
            .setDescription("Example: am!config prefix newPrefix")
            .setFooter(guilds.get(message.guild.id).footer);

        let prefix = args.join(" ").slice(args[0].length + 1);
        if (!prefix) return message.channel.send(correctPrefixEmbed).catch(err => { });
        Servers.findOne({ where: { guildid: message.guild.id } }).then(res => {
            res.update({ prefix: prefix })
            let prefixChangedEmbed = new MessageEmbed()
                .setColor(guilds.get(message.guild.id).embedColor)
                .setTitle("Authme - Prefix changed")
                .setDescription("You have changed the prefix to " + prefix)
                .setFooter(guilds.get(message.guild.id).footer);
            message.channel.send(prefixChangedEmbed).catch(err => { });
        })
    };

    if (args[0] == "embedcolor") {
        let correctPrefixEmbed = new MessageEmbed()
            .setColor(guilds.get(message.guild.id).embedColor)
            .setTitle("Authme - Wrong usage!")
            .setDescription("Example: am!config embedcolor color")
            .setFooter(guilds.get(message.guild.id).footer);

        let prefix = args.join(" ").slice(args[0].length + 1);
        if (!prefix) return message.channel.send(correctPrefixEmbed).catch(err => { });;
        Servers.findOne({ where: { guildid: message.guild.id } }).then(res => {
            res.update({ embedcolor: prefix })
            let prefixChangedEmbed = new MessageEmbed()
                .setColor(guilds.get(message.guild.id).embedColor)
                .setTitle("Authme - Embedcolor changed")
                .setDescription("You have changed the embedcolor to " + prefix)
                .setFooter(guilds.get(message.guild.id).footer);
            message.channel.send(prefixChangedEmbed).catch(err => { });
        })
    };

    if (args[0] == "footer") {
        let correctPrefixEmbed = new MessageEmbed()
            .setColor(guilds.get(message.guild.id).embedColor)
            .setTitle("Authme - Wrong usage!")
            .setDescription("Example: am!config footer New Amazing Footer")
            .setFooter(guilds.get(message.guild.id).footer);

        let prefix = args.join(" ").slice(args[0].length + 1);
        if (!prefix) return message.channel.send(correctPrefixEmbed).catch(err => { });;
        Servers.findOne({ where: { guildid: message.guild.id } }).then(res => {
            res.update({ footer: prefix })
            let prefixChangedEmbed = new MessageEmbed()
                .setColor(guilds.get(message.guild.id).embedColor)
                .setTitle("Authme - Footer changed")
                .setDescription("You have changed the footer to " + prefix)
                .setFooter(guilds.get(message.guild.id).footer);
            message.channel.send(prefixChangedEmbed).catch(err => { });
        })
    };

    if (args[0] == "joinmessage") {
        let correctPrefixEmbed = new MessageEmbed()
            .setColor(guilds.get(message.guild.id).embedColor)
            .setTitle("Authme - Wrong usage!")
            .setDescription("Example: am!config joinmessage New Amazing Join Message")
            .setFooter(guilds.get(message.guild.id).footer);

        let prefix = args.join(" ").slice(args[0].length + 1);
        if (!prefix) return message.channel.send(correctPrefixEmbed).catch(err => { });;
        Roles.findOne({ where: { guildid: message.guild.id } }).then(res => {
            res.update({ joinmessage: prefix })
            let prefixChangedEmbed = new MessageEmbed()
                .setColor(guilds.get(message.guild.id).embedColor)
                .setTitle("Authme - Join Message changed")
                .setDescription("You have changed the joinmessage to " + prefix)
                .setFooter(guilds.get(message.guild.id).footer);
            message.channel.send(prefixChangedEmbed).catch(err => { });
        })
    };

    if (args[0] == "directmessages") {
        let correctPrefixEmbed = new MessageEmbed()
            .setColor(guilds.get(message.guild.id).embedColor)
            .setTitle("Authme - Wrong usage!")
            .setDescription("Example: am!config directmessages true/false")
            .setFooter(guilds.get(message.guild.id).footer);


        let options = [false, true, "true", "false"];
        let dms = args.join(" ").slice(args[0].length + 1);
        if (!dms) return message.channel.send(correctPrefixEmbed).catch(err => { });;
        if (!options.includes(dms)) return message.channel.send(correctPrefixEmbed).catch(err => { });
        let prefix;
        if (dms == "true") {
            prefix = 1
        } else if (dms == "false") {
            prefix = 0
        }
        Servers.findOne({ where: { guildid: message.guild.id } }).then(res => {
            res.update({ dms: prefix })
            let prefixChangedEmbed = new MessageEmbed()
                .setColor(guilds.get(message.guild.id).embedColor)
                .setTitle("Authme - Embedcolor changed")
                .setDescription("You have changed the directmessages to " + dms)
                .setFooter(guilds.get(message.guild.id).footer);
            message.channel.send(prefixChangedEmbed).catch(err => { });
        })
    };

    if (args[0] == "authlogs") {
        let correctPrefixEmbed = new MessageEmbed()
            .setColor(guilds.get(message.guild.id).embedColor)
            .setTitle("Authme - Wrong usage!")
            .setDescription("Example: am!config authlogs true/false")
            .setFooter(guilds.get(message.guild.id).footer);


        let options = [false, true, "true", "false"];
        let authlogs = args.join(" ").slice(args[0].length + 1);
        if (!authlogs) return message.channel.send(correctPrefixEmbed).catch(err => { });;
        if (!options.includes(authlogs)) return message.channel.send(correctPrefixEmbed).catch(err => { });
        let prefix;
        if (authlogs == "true") {
            prefix = 1
        } else if (authlogs == "false") {
            prefix = 0
        }
        Servers.findOne({ where: { guildid: message.guild.id } }).then(res => {
            res.update({ authlogs: prefix })
            let prefixChangedEmbed = new MessageEmbed()
                .setColor(guilds.get(message.guild.id).embedColor)
                .setTitle("Authme - Embedcolor changed")
                .setDescription("You have changed the authlogs to " + authlogs)
                .setFooter(guilds.get(message.guild.id).footer);
            message.channel.send(prefixChangedEmbed).catch(err => { });
        })
    };



};


exports.command = {
    aliases: ["cfg"]
};