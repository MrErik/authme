const { users } = require("./variables");
const { Roles, Servers, Premium } = require("../database/db");

const moment = require("moment");

function checkDate() {
    Premium.findAll({}).then(res => {
        res.forEach(data => {
            if (!data) return
            let info = data.dataValues;
            if (moment().format("YYYY-MM-DD") == moment(info.expires).format("YYYY-MM-DD")) {
                Servers.findOne({ where: { guildid: info.guildid } }).then(res => {
                    res.update({ premium: false })
                });
            }
        })
    })
}

module.exports = { checkDate }; 
