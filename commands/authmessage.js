const { MessageEmbed } = require("discord.js");
const { guilds, roles } = require("../utils/variables");
const { Roles, Servers } = require("../database/db");

exports.use = async (client, message, args, command) => {

    let noPermissionEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("Authme - Missing Permissions")
        .setDescription("You are missing permissions to run this command.")
        .setFooter(guilds.get(message.guild.id).footer);
    if (!message.member.hasPermission("ADMINISTRATOR")) return message.channel.send(noPermissionEmbed).catch(err => { });


    let authmessage = "Please type **am!verify** to verify yourself.";

    if (guilds.get(message.guild.id).premium) {
        if (args[0]) {
            authmessage = args.join(" ");
        }
    }

    let verificationChannel = message.guild.channels.find(ch => ch.name == roles.get(message.guild.id).joinchannel);
    verificationChannel.send(authmessage).then(msg => {
        msg.pin()
      });
      
    message.reply("Authentication message has been posted!");
};

exports.command = {
    aliases: ["authmsg"]
};