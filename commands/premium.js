const { MessageEmbed } = require("discord.js");
const { guilds } = require("../utils/variables");

exports.use = async (client, message, args, command) => {
    let redeemHelpEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe - Premium Help")
        .setDescription("Premium is the better version!")
        .addField("How to purchase premium?", "Click [here](https://store.dropletdev.com/authme/) to purchase premium! Prices:\n**1 Month** - $4\n**3 Months** - $10\n**6 Months** - $18\n**Lifetime** - $35")
        .addField("Why buy premium?", "**1.** Custom role and channel names\n**2.** Change prefix of the bot\n**3.** Change the footer of all embeds\n**4.** Change the embed color of all embeds\n**5.** Disable direct messages from the bot\n**6.** Use beta features\n**7.** Use custom AuthMessage\n**More to come!**")
        .setFooter(guilds.get(message.guild.id).footer);


    message.channel.send(redeemHelpEmbed).catch(err => { });;

};

exports.command = {
    aliases: [""]
}