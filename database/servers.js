const Sequelize = require('sequelize');
const db = require('./connection');

const Servers = db.define('servers', {
    prefix: {
        type: Sequelize.STRING,
        defaultValue: "am!"
    },
    premium: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    footer: {
        type: Sequelize.STRING,
        defaultValue: "Made by Droplet Development - https://dropletdev.com/discord"
    },
    dms: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },
    setup: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    embedcolor: {
        type: Sequelize.STRING,
        defaultValue: "0x731DD8"
    },
    guildid: {
        type: Sequelize.STRING
    },
    authlogs: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
});

module.exports = Servers;