const config = require("../utils/config.json")

let token;
let shardcount;
let clustercount;
let guildspershard = 1700;
if (config.dev) {
    token = config.tokens.devbot
    shardcount = 1;
    clustercount = 1;
} else {
    token = config.tokens.mainbot
    shardcount = 1;
    clustercount = 12;
};
const guilds = new Map();
const users = new Map();
const roles = new Map();
const usingverify = new Map();

const admins = config.admins;

module.exports = { config, token, admins, guilds, users, shardcount, clustercount, guildspershard, roles, usingverify };