const { guilds } = require("../utils/variables.js");
const { errorHandler } = require("../utils/functions.js");
const { Roles, Servers, Premium } = require("../database/db");
const { MessageEmbed } = require("discord.js");

exports.use = async (client, message, args, command) => {
    let noPermissionEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("Missing Permissions")
        .setDescription("You are missing permissions to run this command. You require admin to use the command.")
        .setFooter(guilds.get(message.guild.id).footer);

    if (!message.member.hasPermission("ADMINISTRATOR")) return message.channel.send(noPermissionEmbed);

    const cancelCommand = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe Setup Wizard")
        .setDescription("You have ended this Setup Session. Please type am!setup to setup this bot if you wish to do so later.")
        .setFooter(guilds.get(message.guild.id).footer);

    const setupAM1 = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe Setup Wizard")
        .setDescription("Hello, this is the AuthMe Setup Wizard. When a user joins the server, AuthMe will automatically assign a role to the user that prevents them from seeing any channels except a channel that we will setup later. What do you want your Unverified Role to be called? \n\n1: Unverified\n2: Custom Name\n\nYou may type cancel at any time to exit the setup Wizard.")
        .setFooter(guilds.get(message.guild.id).footer);

    const setupAM2 = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe Setup Wizard")
        .setDescription("The next thing we need to setup is the channel that your users will be able to view when joining the server to verify, please respond with... \n\n1: If you would like to use the default name #verification \n2: If you would like to type a custom name.\n\nYou may type cancel at any time to exit the setup Wizard.")
        .setFooter(guilds.get(message.guild.id).footer);

    const noPremium = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("No Premium")
        .setDescription("This server does not have premium, either claim it on our support server using am!support or\n buy it using am!premium")
        .setFooter(guilds.get(message.guild.id).footer);


    const whatRole = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe Setup Wizard")
        .setDescription("What role would you like to name on your server when you join, use only lowercase please?")
        .setFooter(guilds.get(message.guild.id).footer);


    const whatChannel = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe Setup Wizard")
        .setDescription("What channel would you like to name your server for verification?")
        .setFooter(guilds.get(message.guild.id).footer);


    const whatAuthLog = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe Setup Wizard")
        .setDescription("What channel would you like to name your server for logging the verification?")
        .setFooter(guilds.get(message.guild.id).footer);


    const invalidResponce = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe Setup Wizard")
        .setDescription("The following character you typed is not 1 or 2. Please restart the Setup Wizard.")
        .setFooter(guilds.get(message.guild.id).footer);


    const alreadyExists = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe Setup Wizard")
        .setDescription("There is a role with the name Unverified already. Please delete that role and start again.")
        .setFooter(guilds.get(message.guild.id).footer);

    function setupRoles(message, joinRole, joinChannel, logschannel) { //setupRoles(message, "Unverified", "verification", "authlogs")
        let unverifyRole = message.guild.roles.find(role => role.name === joinRole);
        let logRole = message.guild.roles.find(role => role.name === "AuthChecker");
        if (!unverifyRole) {
            message.guild.roles.create({ data: { name: joinRole } }).then(role => {
                Roles.findOne({ where: { guildid: message.guild.id } }).then(res => {
                    res.update({ joinrole: role.name })
                });

            });
            if (!logRole) { message.guild.roles.create({ data: { name: 'AuthChecker' } }).then(role => { }); }

            const setToRole = new MessageEmbed()
                .setColor(guilds.get(message.guild.id).embedColor)
                .setTitle("AuthMe Setup Wizard")
                .setDescription(`You have set your Unverified Role Name to: "${joinRole}"`)
                .setFooter(guilds.get(message.guild.id).footer);

            message.channel.send(setToRole);
            message.channel.send(setupAM2).then(msg => {

                message.channel.awaitMessages(filter, {
                    max: 1, time: 300000, errors: ['time']
                }).then(collected => {
                    collected = collected.map(x => x.content);
                    collected = collected.toString();
                    if (collected == "cancel") return message.channel.send(cancelCommand);
                    /////////////////////////////////////////////////channel 1, second part
                    if (collected == "1") {
                        try {
                            setupChannel(message, joinRole, "verification", "authlogs");
                        } catch (err) {
                            console.error(err)
                        }

                    } else if (collected == "2") {
                        if (guilds.get(message.guild.id).premium == true) {
                            message.channel.send(whatChannel).then(msg => {

                                message.channel.awaitMessages(filter, {
                                    max: 1, time: 300000, errors: ['time']
                                }).then(collected => {
                                    collected = collected.map(x => x.content);
                                    collected = collected.toString();
                                    if (collected == "cancel") return message.channel.send(cancelCommand);
                                    const verification = collected;
                                    message.channel.send(whatAuthLog).then(msg => {
                                        message.channel.awaitMessages(filter, {
                                            max: 1, time: 300000, errors: ['time']
                                        }).then(collected => {
                                            collected = collected.map(x => x.content);
                                            collected = collected.toString();
                                            if (collected == "cancel") return message.channel.send(cancelCommand);
                                            let authlogs = collected;
                                            try {
                                                setupChannel(message, joinRole, verification, authlogs);
                                            } catch (err) {
                                                console.error(err)
                                            }
                                        });
                                    });
                                });
                            });
                        } else {
                            message.channel.send(noPremium)
                        }
                    } else {
                        message.channel.send(invalidResponce);
                    }
                })
            });
            //-=-=-
        } else return message.channel.send(alreadyExists);

    }

    function setupChannel(message, joinRole, joinChannel, logsChannel) {
        message.guild.channels.forEach(channel => {
            try {
                channel.updateOverwrite(message.guild.roles.find(role => role.name === joinRole), {
                    VIEW_CHANNEL: false
                })
                channel.updateOverwrite(client.user, {
                    VIEW_CHANNEL: true,
                    SEND_MESSAGES: true
                })

            } catch (err) { }
        });

        message.guild.channels.create(joinChannel).then(channel => {
            channel.overwritePermissions({
                permissionOverwrites: [{
                    id: message.guild.roles.find(role => role.name === "@everyone").id,
                    deny: ['VIEW_CHANNEL', 'MENTION_EVERYONE', 'ADD_REACTIONS']
                }, {
                    id: message.guild.roles.find(role => role.name === joinRole).id,
                    allow: ['VIEW_CHANNEL']
                }, {
                    id: client.user.id,
                    allow: ['VIEW_CHANNEL']
                }]

            });
            Roles.findOne({ where: { guildid: message.guild.id } }).then(res => {
                res.update({ joinchannel: channel.name })
            });
        });


        message.guild.channels.create(logsChannel).then(channel => {
            channel.overwritePermissions({
                permissionOverwrites: [{
                    id: message.guild.roles.find(role => role.name === "@everyone").id,
                    deny: ['VIEW_CHANNEL']
                }, {
                    id: message.guild.roles.find(role => role.name === "AuthChecker").id,
                    allow: ['VIEW_CHANNEL']
                }, {
                    id: client.user.id,
                    allow: ['VIEW_CHANNEL']
                }]
            });

            Roles.findOne({ where: { guildid: message.guild.id } }).then(res => {
                res.update({ logschannel: channel.name })
            });
            //Database find the guild and channel id and store it for later
        });

        const setToChannel = new MessageEmbed()
            .setColor(guilds.get(message.guild.id).embedColor)
            .setTitle("AuthMe Setup Wizard")
            .setDescription(`You have set your Verification Channel Name to: "${joinChannel}"`)
            .setFooter(guilds.get(message.guild.id).footer);

        message.channel.send(setToChannel);
        Servers.findOne({ where: { guildid: message.guild.id } }).then(res => {
            res.update({ setup: 1 })
        });
        const setupAM3 = new MessageEmbed()
            .setColor(guilds.get(message.guild.id).embedColor)
            .setTitle("AuthMe Setup Wizard")
            .setDescription(`You have compleated setup of AuthMe. When a user joins the server they will be given the "${joinRole}" role, which will prevent them from seeing any channels except for the "#${joinChannel}" channel. :smile:. \n Confirm by saying anything.`)
            .setFooter(guilds.get(message.guild.id).footer);
        return message.channel.send(setupAM3);
    }



    const filter = m => m.member.id !== client.user.id && m.member.id == message.author.id && m.channel.id == message.channel.id;
    message.channel.send(setupAM1).then(msg => {

        message.channel.awaitMessages(filter, {
            max: 1,
            time: 300000,
            errors: ['time']
        }).then(collected => {

            collected = collected.map(x => x.content);
            collected = collected.toString();

            if (collected == "cancel") return message.channel.send(cancelCommand);
            if (collected == "1") {
                setupRoles(message, "Unverified", "verification", "authlogs")
            } else if (collected == "2") {
                if (guilds.get(message.guild.id).premium == true) {
                    message.channel.send(whatRole).then(msg => {
                        message.channel.awaitMessages(filter, {
                            max: 1, time: 300000, errors: ['time']
                        }).then(collected => {
                            collected = collected.map(x => x.content);
                            collected = collected.toString();
                            if (collected == "cancel") return message.channel.send(cancelCommand);
                            setupRoles(message, collected.toLowerCase(), "verification", "authlogs")
                        });
                    });

                } else {
                    return message.channel.send(noPremium)
                }
            } else {
                message.channel.send(invalidResponce);
            }
        })
    });


};

exports.command = {
    aliases: [""]
};
