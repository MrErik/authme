const { guilds, users, roles, usingverify } = require("../utils/variables.js");
const { errorHandler } = require("../utils/functions.js");
const { Roles, Servers, Votes, Premium } = require("../database/db");
const { MessageEmbed, MessageCollector } = require("discord.js");

const fs = require('fs');
const text2png = require('text2png');
const randomstring = require("randomstring");
const imgurUploader = require('imgur-uploader');

exports.use = async (client, message, args, command) => {
  if (!message.deleted) {
    message.delete();
  }
  let captcha = randomstring.generate(8);
  captcha = captcha.replace("i", "Q")
  captcha = captcha.replace("I", "q")
  captcha = captcha.replace("L", "f")
  captcha = captcha.replace("l", "F")
  fs.writeFileSync('authcodes/' + message.member.id + '.png', text2png(captcha, { color: 'white' }));
  imgurUploader(fs.readFileSync('authcodes/' + message.member.id + '.png'), { title: "Captcha Code" }).then(data => {
    if (typeof usingverify.get(message.author.id) == 'undefined') {
      usingverify.set(message.author.id, false)
    }
    const verifySentEmbed = new MessageEmbed()
      .setColor(guilds.get(message.guild.id).embedColor)
      .setTitle("Verification Message Sent")
      .setDescription("You have been sent a verification message in your private messages. Please go to your private messages and follow the instructions that our bot has given you in private messages.")
      .setFooter(guilds.get(message.guild.id).footer);

    const verifyEmbed = new MessageEmbed()
      .setColor(guilds.get(message.guild.id).embedColor)
      .setTitle("Are you a Human?")
      .setDescription("Please reply to this message with the code in the captcha below")
      .setImage(data.link)
      .setFooter(guilds.get(message.guild.id).footer);

    const invalidCode = new MessageEmbed()
      .setColor(guilds.get(message.guild.id).embedColor)
      .setTitle("Validation Failed")
      .setDescription("The code that you have replied with is incorrect. Please try again shortly. You have a total of three tries.")
      .setFooter(guilds.get(message.guild.id).footer);

    const timedOut = new MessageEmbed()
      .setColor(guilds.get(message.guild.id).embedColor)
      .setTitle("Timed Out")
      .setDescription("You took more than 2 minutes to respond with a verification code. Please try to verify again later.")
      .setFooter(guilds.get(message.guild.id).footer);

    const authlog = new MessageEmbed()
      .setColor(guilds.get(message.guild.id).embedColor)
      .setTitle("Captcha has been solved")
      .setDescription(`User ${message.author.username}, ${message.author.id}, has solved the following code`)
      .setImage(data.link)
      .setFooter(guilds.get(message.guild.id).footer);

    const validDm = new MessageEmbed()
      .setColor(guilds.get(message.guild.id).embedColor)
      .setTitle("Verified")
      .setDescription("You are now able to access the rest of the server as a member.")
      .setFooter(guilds.get(message.guild.id).footer);

    thirdtry = false;
    function finished(msg, captcha, filter) {
      msg.channel.awaitMessages(filter, { max: 1, time: 300000, errors: ['time'] }).then(collected => {
        collected = collected.map(x => x.content);
        collected = collected.toString();
        if (collected == captcha) {
          let role = message.guild.roles.find(r => r.name == roles.get(message.guild.id).joinrole);
          message.member.roles.remove(role);
          let guildlog = message.guild.channels.find(channel => channel.name == roles.get(message.guild.id).logschannel);
          if (guilds.get(message.guild.id).authlogs == true) guildlog.send(authlog);
          if (guilds.get(message.guild.id).dms == true) message.author.send(validDm);
          return usingverify.set(message.author.id, false);
        } else {
          message.author.send(invalidCode);
          const authlogerr = new MessageEmbed()
            .setColor(guilds.get(message.guild.id).embedColor)
            .setTitle("Captcha has been incorrectly solved")
            .setDescription(`User ${message.author.username}, ${message.author.id}, has failed, it may be a bot, and \n insead of answering with ***${captcha}*** answered with ***${collected}***`)
            .setFooter(guilds.get(message.guild.id).footer);
          let guildlog = message.guild.channels.find(channel => channel.name == roles.get(message.guild.id).logschannel);

          if (guilds.get(message.guild.id).dms == true) guildlog.send(authlogerr);

          if (thirdtry == false) {
            const filter = m => m.author.id !== client.user.id && m.author.id == message.author.id;
            finished(msg, captcha, filter)
          } else {
            usingverify.set(message.author.id, false);
            return
          }

          thirdtry = true;
        }


        if (collected.size === 0) {
          collected.on('end', collected => {
            message.channel.send(timedOut);
          });
        }
      })
    }
    if (usingverify.get(message.author.id).usingverify == false) usingverify.set(message.author.id, true)
    if (guilds.get(message.guild.id).setup == false) return message.author.send("This server is not setup yet. Please ask a admin to setup this bot or ask for help using am!support");
    if (message.channel.name !== roles.get(message.guild.id).joinchannel) return message.author.send("You are already verified!");
    message.author.send(verifyEmbed).then(msg => {
      const filter = m => m.author.id !== client.user.id && m.author.id == message.author.id;
      message.channel.send(verifySentEmbed).then(msg => msg.delete(10000));
      msg.channel.awaitMessages(filter, { max: 1, time: 300000, errors: ['time'] }).then(collected => {
        collected = collected.map(x => x.content);
        collected = collected.toString();

        if (collected == captcha) {
          let role = message.guild.roles.find(r => r.name == roles.get(message.guild.id).joinrole);
          message.member.roles.remove(role);
          let guildlog = message.guild.channels.find(channel => channel.name == roles.get(message.guild.id).logschannel);
          if (guilds.get(message.guild.id).authlogs == true) guildlog.send(authlog);
          if (guilds.get(message.guild.id).dms == true) message.author.send(validDm);
          return;
        } else {
          message.author.send(invalidCode);
          let guildlog = message.guild.channels.find(channel => channel.name == roles.get(message.guild.id).logschannel);
          const authlogerr = new MessageEmbed()
          .setColor(guilds.get(message.guild.id).embedColor)
          .setTitle("Captcha has been incorrectly solved")
          .setDescription(`User ${message.author.username}, ${message.author.id}, has failed, it may be a bot, and \n insead of answering with ***${captcha}*** answered with ***${collected}***`)
          .setFooter(guilds.get(message.guild.id).footer);
          guildlog.send(authlogerr);
          const filter = m => m.author.id !== client.user.id && m.author.id == message.author.id;
          finished(msg, captcha, filter);
        }
        if (collected.size === 0) { collected.on('end', collected => { message.channel.send(timedOut)})}
      })
    });
  });
  fs.unlinkSync('authcodes/' + message.member.id + '.png')
};

exports.command = {
  aliases: [""]
}
