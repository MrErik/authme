const { MessageEmbed } = require("discord.js");
const { guilds } = require("../utils/variables");

exports.use = async (client, message, args, command) => {

    const helpEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("AuthMe - Help")
        .setDescription("Need more help than I can provide? Join our discord https://dropletdev.com/discord \nAuthMe commands:\n**am!vote** - Vote for our bot!\n**am!redeem** - Redeem a premium code!!\n**am!help** - You are already here..\n**am!invite** - Invite this amazing bot!\n**am!setup** - Setup AuthMe!.\n**am!stats** - View thoes amazing stats!\n**am!support** - Need help? Run this!\n**am!premium** - See those amazing premium commands!\n**am!config** - Edit the configuration for your server.\n**am!authmessage** - Send the authentication message in the verification channel")
        .setFooter(guilds.get(message.guild.id).footer);

    return message.channel.send(helpEmbed).catch(eam => { });

};

exports.command = {
    aliases: [""]
};