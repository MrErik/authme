// Load Dependencies
const Sequelize = require('sequelize');
const db = require('./connection');

// Load Models
const Roles = require("./roles");
const Servers = require("./servers");
const Premium = require("./premium");


db.sync().catch(error => console.log('Database error:', error));
// Export Models

module.exports = { Roles, Servers, Premium }