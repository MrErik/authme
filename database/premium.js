const Sequelize = require('sequelize');
const db = require('./connection');

const premium = db.define('premium', {
    code: {
        type: Sequelize.STRING,
        allowNull: false
    },
    redeemed: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    expires: {
        type: Sequelize.STRING,
        allowNull: true
    },
    length: {
        type: Sequelize.STRING,
        allowNull: false
    },
    guildid: {
        type: Sequelize.STRING,
        allowNull: true
    },
    clientid: {
        type: Sequelize.STRING,
        allowNull: true
    }
});

module.exports = premium;